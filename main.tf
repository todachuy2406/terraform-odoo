# Connect to Terraform AWS modules
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"
  access_key = "AKIAUBTK2C4SQXRDJ3P6"
  secret_key = "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"
}

variable "subnet_prefix" {
  description = "cidr block for the subnet"
}

### Create a VPC for production
resource "aws_vpc" "vpc-prod" {
 cidr_block = "10.0.0.0/16"
 tags = {
    Name = "production"
  }
}
### Create a subnet for Production
resource "aws_subnet" "subnet-prod" {
  vpc_id     = aws_vpc.vpc-prod.id
  cidr_block = var.subnet_prefix

  tags = {
    Name = "prod-subnet"
  }
}

# #Create a VPC for Dev
# resource "aws_vpc" "vpc-dev" {
#  cidr_block = "10.1.0.0/16"
#  tags = {
#     Name = "dev"
#   }
# }
# # Create a subnet for Dev
# resource "aws_subnet" "subnet-dev" {
#   vpc_id     = aws_vpc.vpc-dev.id
#   cidr_block = "10.1.1.0/24"

#   tags = {
#     Name = "dev-subnet"
#   }
# }

# resource "aws_instance" "cloud-srv1" {
#   ami           = "ami-0443305dabd4be2bc"
#   instance_type = "t2.micro"
#   tags = {
#     #Name = "aws_os"
#   }
# }

# ## Task 1:
# ### 1. Create VPC
# resource "aws_vpc" "vpc-prod" {
#   cidr_block = "10.0.0.0/16"
#   tags = {
#     "Name" = "vpc-PROD"
#   }
# }
# ### 2. Create Internet Gateway
# resource "aws_internet_gateway" "igw" {
#   vpc_id = aws_vpc.vpc-prod.id
# }
# ### 3. Create Custom Route Table
# resource "aws_route_table" "prod-route-table" {
#   vpc_id = aws_vpc.vpc-prod.id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.igw.id
#   }

#   route {
#     ipv6_cidr_block        = "::/0"
#     gateway_id = aws_internet_gateway.igw.id
#   }

#   tags = {
#     Name = "rt-PROD"
#   }
# }
# ### 4. Create a subnet
# resource "aws_subnet" "subnet-prod" {
#   vpc_id = aws_vpc.vpc-prod.id
#   cidr_block = "10.0.1.0/24"
#   availability_zone = "us-east-2a"
#   tags = {
#     "Name" = "subnet-PROD"
#   }
# }
# ### 5. Associate subnet with Route Table
# resource "aws_route_table_association" "a" {
#   subnet_id      = aws_subnet.subnet-prod.id
#   route_table_id = aws_route_table.prod-route-table.id
# }
# ### 6. Create Security Group to allow port 22, 80, 443
# resource "aws_security_group" "allow_web" {
#   name        = "allow_web_traffic"
#   description = "Allow Web inbound traffic"
#   vpc_id      = aws_vpc.vpc-prod.id

#   ingress {
#       description      = "HTTPS"
#       from_port        = 443
#       to_port          = 443
#       protocol         = "tcp"
#       cidr_blocks      = ["0.0.0.0/0"]
#       ipv6_cidr_blocks = ["0.0.0.0/0"]
#   }
  
#   ingress {
#       description      = "HTTP"
#       from_port        = 80
#       to_port          = 80
#       protocol         = "tcp"
#       cidr_blocks      = ["0.0.0.0/0"]
#       ipv6_cidr_blocks = ["0.0.0.0/0"]
#   }
  
#   ingress {
#       description      = "SSH"
#       from_port        = 22
#       to_port          = 22
#       protocol         = "tcp"
#       cidr_blocks      = ["0.0.0.0/0"]
#       ipv6_cidr_blocks = ["0.0.0.0/0"]
#   }
  
#   egress {
#       from_port        = 0
#       to_port          = 0
#       protocol         = "-1"
#       cidr_blocks      = ["0.0.0.0/0"]
#       ipv6_cidr_blocks = ["::/0"]
#   }
  
#   tags = {
#     Name = "allow_web"
#   }
# }
# ### 7. Create a network interface with an IP in the subnet that was created in step 4
# resource "aws_network_interface" "nic-web-server" {
#   subnet_id       = aws_subnet.subnet-prod.id
#   private_ips     = ["10.0.1.50"]
#   security_groups = [aws_security_group.allow_web.id]

# }
# ### 8. Assign an elastic IP to the network interface created in step 7
# resource "aws_eip" "one" {
#   instance                  = aws_instance.web-server-instance.id
#   vpc                       = true
#   network_interface         = aws_network_interface.nic-web-server.id
#   associate_with_private_ip = "10.0.1.50"
#   depends_on = [aws_internet_gateway.igw]
# }

# output "public_ip" {
#   value = aws_eip.one.public_ip
  
# }

# ### 9. Create Ubuntu Server and install/enable apache2
# resource "aws_instance" "web-server-instance" {
#   ami = "ami-00399ec92321828f5"
#   instance_type = "t2.micro"
#   availability_zone = "us-east-2a"
#   key_name = "learnAWS01"

#   network_interface {
#     device_index = 0
#     network_interface_id = aws_network_interface.nic-web-server.id
#   }

#   user_data = <<-EOF
#               #!/bin/bash
#               sudo apt update -y
#               sudo apt install apache2 -y
#               sudo systemctl start apach2
#               sudo bash -c "echo your very first web server > /var/www/html/index.html"
#               EOF
  
#   tags = {
#     "Name" = "web-server"
#   }
  
# }

# output "server_private_ip" {
#   value = aws_instance.web-server-instance.private_ip
# }

# output "server_instance_id" {
#   value = aws_instance.web-server-instance.id  
# }
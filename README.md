# Terraform SAP
## Terraform CLI:
- terraform init
- terraform plan
- terraform apply
- terraform apply --auto-approve
- terraform refresh
- terraform destroy -target aws_instance.web-server-instance
- terraform apply -target aws_instance.web-server-instance

## Install Terraform in Windows 10
- Download compress file: https://releases.hashicorp.com/terraform/1.0.4/terraform_1.0.4_windows_amd64.zip
- Uncompress to C:\terraform
- Start -> type "env" -> Edit the system environments variables -> Environments Variables -> Edit Path on System Variables part -> Add C:\terraform entry -> OK

## Get Access Key AWS:
- Connect AWS console -> My Security Credentials -> Access Keys -> Create new access key (copy and download file)

## Make a main.tf file into Project directory add the following lines:

Configure to Terraform AWS modules
```
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
```

Configure the AWS Provider
```
provider "aws" {
  region = "us-east-2"
  access_key = "paste_access_key"
  secret_key = "paste_secret_key"
}
```

Create a VPC for production
```
resource "aws_vpc" "vpc-prod" {
 cidr_block = "10.0.0.0/16"
 tags = {
    Name = "production"
  }
}
```
Create a subnet for Production
```
resource "aws_subnet" "subnet-prod" {
  vpc_id     = aws_vpc.vpc-prod.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "prod-subnet"
  }
}
```
## Task 1:
### 1. Create VPC
```
resource "aws_vpc" "vpc-prod" {
  cidr_block = "10.0.0.0/16"
  tags = {
    "Name" = "vpc-PROD"
  }
}
```
### 2. Create Internet Gateway
```
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc-prod.id
}
```
### 3. Create Custom Route Table
```
resource "aws_route_table" "prod-route-table" {
  vpc_id = aws_vpc.vpc-prod.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "rt-PROD"
  }
}
```
### 4. Create a subnet
```
resource "aws_subnet" "subnet-prod" {
  vpc_id = aws_vpc.vpc-prod.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-2a"
  tags = {
    "Name" = "subnet-PROD"
  }
}
```
### 5. Associate subnet with Route Table
```
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-prod.id
  route_table_id = aws_route_table.prod-route-table.id
}
```
### 6. Create Security Group to allow port 22, 80, 443
```
resource "aws_security_group" "allow_web" {
  name        = "allow_web_traffic"
  description = "Allow Web inbound traffic"
  vpc_id      = aws_vpc.vpc-prod.id

  ingress {
      description      = "HTTPS"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
      description      = "HTTP"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
      description      = "SSH"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
  }
  
  tags = {
    Name = "allow_web"
  }
}
```
### 7. Create a network interface with an IP in the subnet that was created in step 4
```
resource "aws_network_interface" "nic-web-server" {
  subnet_id       = aws_subnet.subnet-prod.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_web.id]

}
```
### 8. Assign an elastic IP to the network interface created in step 7
```
resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.nic-web-server.id
  associate_with_private_ip = "10.0.1.50"
  depends_on = [aws_internet_gateway.igw]
}

output "public_ip" {
  value = aws_eip.one.public_ip
  
}
```
### 9. Create Ubuntu Server and install/enable apache2
```
resource "aws_instance" "web-server-instance" {
  ami = "ami-00399ec92321828f5"
  instance_type = "t2.micro"
  availability_zone = "us-east-2a"
  key_name = "learnAWS01"

  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.nic-web-server.id
  }

  user_data = <<-EOF
              #!/bin/bash
              sudo apt update -y
              sudo apt install apache2 -y
              sudo systemctl start apach2
              sudo bash -c "echo your very first web server > /var/www/html/index.html"
              EOF
  
  tags = {
    "Name" = "web-server"
  }
  
}

output "server_private_ip" {
  value = aws_instance.web-server-instance.private_ip
}

output "server_instance_id" {
  value = aws_instance.web-server-instance.id  
}
```
## Preferences:
- Youtube: https://www.youtube.com/watch?v=SLB_c_ayRMo&t=5232s